<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CourseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('courses')->insert(array(
            array(
                'name' => 'English'
            ),
            array(
                'name' => 'French'
            ),
            array(
                'name' => 'ICDL'
            ),
            array(
                'name' => 'Communication Skills'
            ),
        ));
    }
}
