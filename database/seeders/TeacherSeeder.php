<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TeacherSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('teachers')->insert(array(
        array(
            'name' => 'Gary Cabrera'
        ),
        array(
            'name' => ' James Vance'
        ),
        array(
            'name' => 'Aliza Vance'
        ),
        array(
            'name' => 'Averie Carter'
        ),

    ));
    }
}
