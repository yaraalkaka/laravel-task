<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CourseTeacherSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('course_teachers')->insert(array(
            array(
                'course_id' => 1,
                'teacher_id' =>1
            ),
            array(
                'course_id' => 1,
                'teacher_id' =>2
            ),
            array(
                'course_id' => 2,
                'teacher_id' =>3
            ),
            array(
                'course_id' => 2,
                'teacher_id' =>4
            ),
            array(
                'course_id' => 3,
                'teacher_id' =>2
            ),
            array(
                'course_id' => 4,
                'teacher_id' =>3
            ),
        ));
    }
}
