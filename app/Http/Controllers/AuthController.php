<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    public function register(Request $request):JsonResponse
    {
        $request->validate([
            'name' =>['required', 'max:55', 'regex:/^[\pL\s]+$/u', 'string'],
            'email' => ['email','required','unique:users'],
            'password' => [
                'required',
                'confirmed',
            ]
        ]);


        $user = User::query()->create($request->all());
        if(!$user){
            return response()->json([
                'success' => false,
                'message'=> 'Registration failed'
            ]);
        }
        $accessToken = $user->createToken('Personal Access Token')->accessToken;
        $user['password'] = bcrypt($request['password']);
        $user->save();
        $user['remember_token']= $accessToken;

        return response()->json([
            'user' => $user,
            'access_token' => $accessToken
        ]);
    }

    public function login(Request $request): JsonResponse
    {
        $loginData = $request->validate([
            'email' => 'email|required|exists:users',
            'password' => 'required'
        ]);
        if (!auth()->attempt($loginData)) {
            return response()->json([
                'errors' => [
                    'message' => ['could not sign you in with those credentials']
                ]
            ], 422);
        }
        $user = $request->user();
        $accessToken = $user->createToken('Personal Access Token');
        $user['remember_token']= $accessToken;
        $accessToken->token->save();
        return response()->json([
            'data' => $user,
            'access_token' => $accessToken->accessToken,
            'token_type' => 'Bearer',
        ]);
    }


    public function logout(): JsonResponse
    {
        $user = Auth::user()->token();
        $user->revoke();
        return response()->json(['success' => 'logged out successfully']);

    }

}
