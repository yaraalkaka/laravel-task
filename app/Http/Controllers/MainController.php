<?php

namespace App\Http\Controllers;

use App\Models\Course;
use App\Models\Course_teacher;
use App\Models\Student;
use App\Models\Student_course;
use App\Models\Teacher;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class MainController extends Controller
{
    public function index(): JsonResponse
    {
        $students = Student::query()
            ->with([
                'student_course.course_teacher.teacher',
                'student_course.course_teacher.course'
            ])
            ->get();
        return response()->json(['students' => $students]);
    }//list of all students

    public function store(Request $request): JsonResponse
    {
        $request->validate([
            'name'=> ['string', 'max:55', 'regex:/^[\pL\s]+$/u','required', 'unique:students,name'],
            'chosen' => ['required','array'],
            'chosen.*.course'=>['required', 'string','max:55', 'unique:courses,name'],
            'chosen.*.teacher'=>['required', 'string','max:55','regex:/^[\pL\s]+$/u', 'unique:teachers,name'],
        ]);
        //create unique student
        $student = Student::query()->create([
            'name'=> $request['name'],
        ]);

        //create courses if not exist
        $i=0;
        $courses = [];
        foreach ($request['chosen'] as $choice){
            $check_if = Course::query()
                ->where('name', '=', $choice['course']);
            if($check_if->exists()){
                $courses[$i] = $check_if->first()['id'];
            }else{
                $courses[$i] = Course::query()->create([
                    'name'=> $choice['course'],
                ])['id'];
            }
            $i++;
        }

        //create teachers if not exist
        $j=0;
        $teachers = [];
        foreach ($request['chosen'] as $choice){
            $check_if = Teacher::query()
                ->where('name', '=', $choice['teacher']);
            if($check_if->exists()){
                $teachers[$j] = $check_if->first()['id'];
            }else{
                $teachers[$j] = Teacher::query()->create([
                    'name'=> $choice['teacher']
                ])['id'];
            }
            $j++;
        }
        //assign teachers to courses
        $course_teachers = [];
        for ($k=0;$k<$i;$k++){
            $check_if = Course_teacher::query()
                ->where('course_id', '=', $courses[$k])
                ->where('teacher_id', '=',$teachers[$k]);
            if($check_if->exists()){
                $course_teachers[$k] = $check_if->first()['id'];
            }else{
                $course_teachers[$k] = Course_teacher::query()->create([
                    'course_id' => $courses[$k],
                    'teacher_id' => $teachers[$k]
                ])['id'];
            }
            $k++;
        }
        //register student to courses
        $student_courses = [];
        for ($l=0;$l<$i;$l++){
            $check_if = Student_course::query()
                ->where('course_teacher_id', '=', $course_teachers[$l])
                ->where('student_id', '=',$student['id']);
            if($check_if->exists()){
                $student_courses[$l] = $check_if->first()['id'];
            }else{
                $student_courses[$l] = Student_course::query()->create([
                    'course_teacher_id' => $course_teachers[$l],
                    'student_id' => $student['id']
                ])['id'];
            }
            $l++;
        }

        if($student->save()){
            return response()->json($student)->setStatusCode(200);
        }else{
            return response()->json(['message'=>'Store Failed']);
        }
    }//store a new student

    public function update(Request $request, $id): JsonResponse{
        $request->validate([
            'name'=> ['string', 'max:55', 'regex:/^[\pL\s]+$/u'],
            'chosen' => ['required','array'],
            'chosen.*.course'=>['required', 'string','max:55', 'unique:courses,name'],
            'chosen.*.teacher'=>['required', 'string','max:55','regex:/^[\pL\s]+$/u', 'unique:teachers,name'],
        ]);
        if($request->has('name')){
            Student::query()->find($id)->update([
               'name' => $request['name']
            ]);
        }
        //delete student courses
        Student_course::query()->where('student_id','=',$id)->delete();

        //add new student courses

        //create courses if not exist
        $i=0;
        $courses = [];
        foreach ($request['chosen'] as $choice){
            $check_if = Course::query()
                ->where('name', '=', $choice['course']);
            if($check_if->exists()){
                $courses[$i] = $check_if->first()['id'];
            }else{
                $courses[$i] = Course::query()->create([
                    'name'=> $choice['course'],
                ])['id'];
            }
            $i++;
        }

        //create teachers if not exist
        $j=0;
        $teachers = [];
        foreach ($request['chosen'] as $choice){
            $check_if = Teacher::query()
                ->where('name', '=', $choice['teacher']);
            if($check_if->exists()){
                $teachers[$j] = $check_if->first()['id'];
            }else{
                $teachers[$j] = Teacher::query()->create([
                    'name'=> $choice['teacher']
                ])['id'];
            }
            $j++;
        }
        //assign teachers to courses
        $course_teachers = [];
        for ($k=0;$k<$i;$k++){
            $check_if = Course_teacher::query()
                ->where('course_id', '=', $courses[$k])
                ->where('teacher_id', '=',$teachers[$k]);
            if($check_if->exists()){
                $course_teachers[$k] = $check_if->first()['id'];
            }else{
                $course_teachers[$k] = Course_teacher::query()->create([
                    'course_id' => $courses[$k],
                    'teacher_id' => $teachers[$k]
                ])['id'];
            }
            $k++;
        }
        //register student to courses
        $student_courses = [];
        for ($l=0;$l<$i;$l++){
            $check_if = Student_course::query()
                ->where('course_teacher_id', '=', $course_teachers[$l])
                ->where('student_id', '=',$id);
            if($check_if->exists()){
                $student_courses[$l] = $check_if->first()['id'];
            }else{
                $student_courses[$l] = Student_course::query()->create([
                    'course_teacher_id' => $course_teachers[$l],
                    'student_id' => $id
                ])['id'];
            }
            $l++;
        }

        $student = Student::query()->find($id);
        return response()->json(['student' => $student]);
    }//update an existing student

}
