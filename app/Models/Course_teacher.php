<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Course_teacher extends Model
{
    use HasFactory;
    protected $fillable = [
        'course_id',
        'teacher_id',
    ];

    public function course(): BelongsTo
    {
        return $this->belongsTo(Course::class);
    }
    public function teacher(): BelongsTo
    {
        return $this->belongsTo(Teacher::class);
    }

    /**
     * My PK is FK where
     */
    public function student_course(): HasMany
    {
        return $this->hasMany(Student_course::class);
    }
}
