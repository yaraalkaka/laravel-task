<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Course extends Model
{
    use HasFactory;
    protected $fillable = [
      'name'
    ];

    /**
     * My PK is FK where
     */

    public function student_course(): HasMany
    {
        return $this->hasMany(Student_course::class);
    }
}
