<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Student_course extends Model
{
    use HasFactory;

    protected $fillable = [
        'course_teacher_id',
        'student_id',
    ];
    /**
     * My FK belongs to
     */
    public function student(): BelongsToMany
    {
        return $this->belongsToMany(Student::class);
    }

    public function course_teacher(): BelongsTo
    {
        return $this->belongsTo(Course_teacher::class);
    }
}
