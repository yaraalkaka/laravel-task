<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Teacher extends Model
{
    use HasFactory;
    protected $fillable = ['name'];

    /**
     * My PK is FK where
     */
    public function course_teacher(): HasMany
    {
        return $this->hasMany(Course_teacher::class);
    }
}
